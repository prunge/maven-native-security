package au.net.causal.maven.nativesecurity;

import java.util.Map;

import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.sonatype.plexus.components.sec.dispatcher.PasswordDecryptor;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcherException;

import au.net.causal.maven.nativesecurity.encrypter.EncryptionException;
import au.net.causal.maven.nativesecurity.encrypter.NativeEncrypter;
import au.net.causal.maven.nativesecurity.encrypter.NativeEncrypterManager;

//Use this by putting:
//{[type=native]EnCryPteDpAsSwOrD=}
//in your settings.xml and it should just work

/**
 * A Maven password decrypter that uses the native system's capabilities for decrypting passwords rather than the 
 * <a href="https://maven.apache.org/guides/mini/guide-encryption.html">standard master password</a> system.
 * <p>
 * 
 * To use this decrypter when it is registered, use something like the following in <code>settings.xml</code> for encrypted passwords:
 * <p>
 * 
 * <code>{[type=native]EnCryPteDpAsSwOrD=}</code>
 * 
 * @author prunge
 */
@Component(role=PasswordDecryptor.class, hint="native")
public class NativeSecurityPasswordDecrypter
extends AbstractLogEnabled
implements PasswordDecryptor
{
	@Requirement
	private NativeEncrypterManager encrypterManager;
	
	@Override
	public String decrypt(String str, Map attributes, Map config) throws SecDispatcherException
	{
		NativeEncrypter encrypter = encrypterManager.findUsableEncrypter();
		if (encrypter == null)
			throw new SecDispatcherException("No native encrypters available for current platform.");
		
		getLogger().debug("Using native encrypter: " + encrypter);
		
		try
		{
			String password = encrypter.decrypt(str);
			return(password);
		}
		catch (EncryptionException e)
		{
			throw new SecDispatcherException(e);
		}
	}
}
