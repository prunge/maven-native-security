package au.net.causal.maven.nativesecurity.encrypter;

import java.nio.charset.Charset;
import java.security.SecureRandom;

import org.codehaus.plexus.component.annotations.Requirement;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.codehaus.plexus.util.Base64;
import org.sonatype.plexus.components.cipher.PlexusCipher;
import org.sonatype.plexus.components.cipher.PlexusCipherException;

/**
 * Base for native encrypters that are more like a key-store rather than encrypters themselves.
 * <p>
 * 
 * Key-based native encrypters will store a one-time generated password as a master key, then that password will be used to encrypt further passwords
 * similarly to how <a href="https://maven.apache.org/guides/mini/guide-encryption.html">Maven's master password</a> works.
 * 
 * @author prunge
 */
public abstract class KeyBasedNativeEncrypter extends AbstractLogEnabled 
implements NativeEncrypter
{
	/**
	 * Name of the master key for storing the generated master key's password in the key-based store.
	 */
	protected static final String MASTER_KEY_NAME = "au.net.causal.maven.nativesecurity.MasterKey";
	
	/**
	 * The number of random bytes to generate for the master password.
	 */
	private static final int MASTER_PASSWORD_NUM_BYTES = 32;
	
	private static final Charset UTF_8 = Charset.forName("UTF-8");

	@Override
	public String encrypt(String password) 
	throws EncryptionException
	{
		if (password == null)
			throw new NullPointerException("password == null");
		
		String masterKey = readMasterKey();
		if (masterKey == null)
			masterKey = generateAndSaveMasterKey();
		
		try
		{
			String encryptedBase64Password = getCipher().encrypt(password, masterKey);
			return(encryptedBase64Password);
		}
		catch (PlexusCipherException e)
		{
			throw new EncryptionException("Error encrypting password: " + e.getMessage(), e);
		}
	}
	
	@Override
	public String decrypt(String base64EncodedPassword) 
	throws EncryptionException
	{
		if (base64EncodedPassword == null)
			throw new NullPointerException("base64EncodedPassword == null");
		
		String masterKey = readMasterKey();
		if (masterKey == null)
			throw new EncryptionException("No master key exists using " + getClass().getCanonicalName() + ", so cannot decrypt anything.");
		
		try
		{
			return(getCipher().decrypt(base64EncodedPassword, masterKey));
		}
		catch (PlexusCipherException e)
		{
			throw new EncryptionException("Error decrypting password: " + e.getMessage(), e);
		}
	}
	
	private String generateAndSaveMasterKey()
	throws EncryptionException
	{
		String masterKey = generateMasterKey();
		saveMasterKey(masterKey);
		return(masterKey);
	}
	
	/**
	 * Generate a new random master key.
	 * 
	 * @return the generated master key.
	 */
	protected String generateMasterKey()
	{
		//This is not done often so only instantiate SecureRandom here
		SecureRandom random = new SecureRandom();
		byte[] passBytes = new byte[MASTER_PASSWORD_NUM_BYTES];
		random.nextBytes(passBytes);
		
		byte[] base64Bytes = Base64.encodeBase64(passBytes);
		String masterKey = new String(base64Bytes, UTF_8);
		
		return(masterKey);
	}

	/**
	 * Reads the master key from the store.
	 * 
	 * @return the master key, or <code>null</code> if none exists.
	 * 
	 * @throws EncryptionException if an error occurs accessing the store.
	 */
	protected abstract String readMasterKey()
	throws EncryptionException;
	
	/**
	 * Saves a new master key into the store.
	 * 
	 * @param masterKey the master key to save.
	 * 
	 * @throws NullPointerException if <code>masterKey</code> is null.
	 * @throws EncryptionException if an error occurs saving to the store.
	 */
	protected abstract void saveMasterKey(String masterKey)
	throws EncryptionException;
	
	/**
	 * Returns the cipher to use for encrypting the master key. 
	 * <p>
	 * 
	 * Most subclasses should simply have this injected using {@literal @}{@link Requirement} and then return it in their getter.
	 * 
	 * @return the cipher to use for encrypting with the master key.
	 */
	protected abstract PlexusCipher getCipher();
}
