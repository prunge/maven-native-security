package au.net.causal.maven.nativesecurity.encrypter;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

@SuppressWarnings("javadoc")
public interface GnomeKeyringLibrary extends Library
{
    /*GnomeKeyringItemType*/
	int GNOME_KEYRING_ITEM_GENERIC_SECRET = 0;
	
	// GnomeKeyringAttributeList gnome_keyring_attribute_list_new() = g_array_new(FALSE, FALSE, sizeof(GnomeKeyringAttribute))
    int GnomeKeyringAttribute_SIZE = Pointer.SIZE * 3; // conservatively: 2 pointers + 1 enum
	
    //Errors
    int GNOME_KEYRING_RESULT_OK = 0;
    int GNOME_KEYRING_RESULT_DENIED = 1;
    int GNOME_KEYRING_RESULT_NO_KEYRING_DAEMON = 2;
    int GNOME_KEYRING_RESULT_ALREADY_UNLOCKED = 3;
    int GNOME_KEYRING_RESULT_NO_SUCH_KEYRING = 4;
    int GNOME_KEYRING_RESULT_BAD_ARGUMENTS = 5;
    int GNOME_KEYRING_RESULT_IO_ERROR = 6;
    int GNOME_KEYRING_RESULT_CANCELLED = 7;
    int GNOME_KEYRING_RESULT_KEYRING_ALREADY_EXISTS = 8;
    int GNOME_KEYRING_RESULT_NO_MATCH = 9;
    
	/**
	 * @see <a href="https://developer.gnome.org/gnome-keyring/stable/gnome-keyring-Miscellaneous-Functions.html#gnome-keyring-is-available">gnome_keyring_is_available()</a>
	 */
	boolean gnome_keyring_is_available();

    /** 
     * @see <a href="https://developer.gnome.org/glib/2.37/glib-Miscellaneous-Utility-Functions.html#g-set-application-name">g_set_application_name()</a>
     */
    void g_set_application_name(String name);

    /**
     * @see <a href="https://developer.gnome.org/glib/2.37/glib-Arrays.html#g-array-new">g_array_new()</a>
     */
    Pointer g_array_new(boolean zero_terminated, boolean clear, int element_size);
    
    /** http://library.gnome.org/devel/glib/2.6/glib-Doubly-Linked-Lists.html */

    /**
     * @see <a href="https://developer.gnome.org/glib/2.37/glib-Doubly-Linked-Lists.html#g-list-length">g_list_length()</a>
     */
    int g_list_length(Pointer list);
    
    /**
     * @see <a href="https://developer.gnome.org/glib/2.37/glib-Doubly-Linked-Lists.html#g-list-nth-data">g_list_nth_data()</a>
     */
    /*gpointer*/ GnomeKeyringFound g_list_nth_data(
            Pointer list,
            int n);
    
    /**
     * @see <a href="https://developer.gnome.org/gnome-keyring/stable/gnome-keyring-Item-Attributes.html#gnome-keyring-attribute-list-append-string">gnome_keyring_attribute_list_append_string()</a>
     */
    void gnome_keyring_attribute_list_append_string(
    			/*GnomeKeyringAttributeList*/Pointer attributes,
	            String name,
	            String value);
    
    /**
     * @see <a href="https://developer.gnome.org/gnome-keyring/stable/gnome-keyring-Item-Attributes.html#gnome-keyring-attribute-list-free">gnome_keyring_attribute_list_free()</a>
     */
    void gnome_keyring_attribute_list_free(/*GnomeKeyringAttributeList*/Pointer attributes);

    /**
     * @see <a href="https://developer.gnome.org/gnome-keyring/stable/gnome-keyring-Keyring-Items.html#gnome-keyring-item-create-sync">gnome_keyring_item_create_sync()</a>
     */
    int gnome_keyring_item_create_sync(
            String keyring,
            /*GnomeKeyringItemType*/int type,
            String display_name,
            /*GnomeKeyringAttributeList*/Pointer attributes,
            String secret,
            boolean update_if_exists,
            int[] item_id);
	
    /**
     * @see <a href="https://developer.gnome.org/gnome-keyring/stable/gnome-keyring-Search-Functionality.html#gnome-keyring-find-items-sync">gnome_keyring_find_items_sync()</a>
     */
    int gnome_keyring_find_items_sync(
            /*GnomeKeyringItemType*/int type,
            /*GnomeKeyringAttributeList*/Pointer attributes,
            /*GList<GnomeKeyringFound>*/Pointer[] found);

    /**
     * @see <a href="https://developer.gnome.org/gnome-keyring/stable/gnome-keyring-Search-Functionality.html#gnome-keyring-found-list-free">gnome_keyring_found_list_free()</a>
     */
    void gnome_keyring_found_list_free(
            /*GList<GnomeKeyringFound>*/Pointer found_list);

    /**
     * @see <a href="https://developer.gnome.org/gnome-keyring/stable/gnome-keyring-Keyring-Items.html#gnome-keyring-item-delete-sync">gnome_keyring_item_delete_sync</a>
     */
    int gnome_keyring_item_delete_sync(
            String keyring,
            int id);
    
    class GnomeKeyringFound extends Structure 
    {
        public String keyring;
        public int item_id;
        public /*GnomeKeyringAttributeList*/Pointer attributes;
        public String secret;

        @Override
        public List<String> getFieldOrder() 
        {
        	return Arrays.asList("keyring", "item_id", "attributes", "secret");
        }
    }
}
