package au.net.causal.maven.nativesecurity.encrypter;

/**
 * Encrypts passwords using native operating system facilities.
 * 
 * @author prunge
 */
public interface NativeEncrypter
{
	/**
	 * Plexus component role.
	 */
	public static final String ROLE = NativeEncrypter.class.getName();
	
	/**
	 * Encrypt a password.
	 * 
	 * @param password password to encrypt.
	 * 
	 * @return the encrypted password in base64 format.
	 * 
	 * @throws NullPointerException if <code>password</code> is null.
	 * @throws EncryptionException if an error occurs performing encryption.
	 */
	public String encrypt(String password)
	throws EncryptionException;
	
	/**
	 * Decrypts a password.
	 * 
	 * @param base64EncodedPassword the base64 encoded password to decrypt.
	 * 
	 * @return the password.
	 * 
	 * @throws NullPointerException if <code>base64EncodedPassword</code> is null.
	 * @throws EncryptionException if an error occurs performing decryption.
	 */
	public String decrypt(String base64EncodedPassword)
	throws EncryptionException;
	
	/**
	 * Returns whether the native encrypter can be used in the current environment.
	 * <p>
	 * 
	 * Typically a native encrypter will require facilities from the environment or operating system.  For example, a Windows DPAPI encrypter would 
	 * require being run on the Windows operating system.
	 * 
	 * @return true if the encrypter can be used, false if not.
	 */
	public boolean isUsable();
	
	/**
	 * Returns details containing the reason the encrypter is not usable on the platform.  May return null if no info is available.  Must return null
	 * if the encrypter is usable.
	 * 
	 * @return a usability detail string, or null.
	 * 
	 * @see #isUsable()
	 */
	public UsabilityDetail getUsabilityDetail();
	
	/**
	 * Details for when an encrypter is not usable on the current platform.
	 * Can contain a detail message and optionally an exception.
	 * 
	 * @author prunge
	 */
	public static class UsabilityDetail
	{
		private final String summary;
		private final String detailMessage;
		private final Throwable error;
		
		/**
		 * Creates a <code>UsabilityDetail</code> with just a summary message.
		 * 
		 * @param summary the summary message.
		 */
		public UsabilityDetail(String summary)
		{
			this(summary, null, null);
		}
		
		/**
		 * Creates a <code>UsabilityDetail</code> with a summary and detail message.
		 * 
		 * @param summary the summary.
		 * @param detailMessage the detail message.
		 */
		public UsabilityDetail(String summary, String detailMessage)
		{
			this(summary, detailMessage, null);
		}
		
		/**
		 * Creates a <code>UsabilityDetail</code> with a summary and cause exception with the detail message derived from the exception.
		 * 
		 * @param summary the summary.
		 * @param error the cause exception.
		 */
		public UsabilityDetail(String summary, Throwable error)
		{
			this(summary, error.getMessage(), error);
		}
		
		/**
		 * Creates a <code>UsabilityDetail</code>.
		 * 
		 * @param summary the summary.
		 * @param detailMessage the detail message.
		 * @param error the cause exception.
		 */
		public UsabilityDetail(String summary, String detailMessage, Throwable error)
		{
			this.summary = summary;
			this.detailMessage = detailMessage;
			this.error = error;
		}
		
		/**
		 * @return a short summary of why the encrypter is not available.
		 */
		public String getSummary()
		{
			return(summary);
		}

		/**
		 * @return a longer description explaining why the encrypter is not available.
		 */
		public String getDetailMessage()
		{
			return(detailMessage);
		}

		/**
		 * @return any exception that explains why the encrypter is not available.
		 */
		public Throwable getError()
		{
			return(error);
		}
	}
}
