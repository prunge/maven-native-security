package au.net.causal.maven.nativesecurity.encrypter;

/**
 * Occurs when something goes wrong with encryption or decryption.
 * 
 * @author prunge
 */
public class EncryptionException extends Exception
{
	/**
	 * Creates an <code>EncryptionException</code>.
	 */
	public EncryptionException()
	{
	}

	/**
	 * Creates an <code>EncryptionException</code> with a detail message.
	 * 
	 * @param message the detail message.
	 */
	public EncryptionException(String message)
	{
		super(message);
	}

	/**
	 * Creates an <code>EncryptionException</code> with a cause exception.
	 * 
	 * @param cause the cause exception.
	 */
	public EncryptionException(Throwable cause)
	{
		super(cause);
	}

	/**
	 * Creates an <code>EncryptionException</code> with a detail message and cause exception.
	 * 
	 * @param message the detail message.
	 * @param cause the cause exception.
	 */
	public EncryptionException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
