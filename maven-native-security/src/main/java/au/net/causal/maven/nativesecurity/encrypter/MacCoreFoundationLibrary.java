package au.net.causal.maven.nativesecurity.encrypter;

import au.net.causal.maven.nativesecurity.encrypter.MacNativeTypes.CFIndex;
import au.net.causal.maven.nativesecurity.encrypter.MacNativeTypes.CFRange;
import au.net.causal.maven.nativesecurity.encrypter.MacNativeTypes.CFStringRef;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.PointerType;

/**
 * @see <a href="https://developer.apple.com/library/mac/documentation/corefoundation/Reference/CFTypeRef/Reference/reference.html">CoreFoundation documentation</a>
 */
public interface MacCoreFoundationLibrary extends Library
{	
	CFIndex CFStringGetLength(CFStringRef theString);
	void CFStringGetCharacters(CFStringRef theString, CFRange.ByValue range, char[] buffer);
	
	void CFRelease(Pointer pointer);
	void CFRelease(PointerType pointer);
}
