package au.net.causal.maven.nativesecurity.encrypter;

import java.util.List;

/**
 * Allows native encrypters to be looked up and used.
 * 
 * @author prunge
 * 
 * @see NativeEncrypter
 */
public interface NativeEncrypterManager
{
	/**
	 * Plexus component role.
	 */
	public static final String ROLE = NativeEncrypterManager.class.getName();
	
	/**
	 * From all registered <code>NativeEncrypter</code> components, find the first one that is usable in the current environment and return it.
	 * 
	 * @return the first usable native encrypter, or <code>null</code> if none could be found.
	 */
	public NativeEncrypter findUsableEncrypter();
	
	/**
	 * @return a list of all installed encrypters.
	 */
	public List<NativeEncrypter> getEncrypters();
}
