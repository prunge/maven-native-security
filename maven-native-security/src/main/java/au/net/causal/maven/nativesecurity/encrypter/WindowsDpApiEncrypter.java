package au.net.causal.maven.nativesecurity.encrypter;

import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.util.Os;

import com.sun.jna.platform.win32.Crypt32Util;
import com.sun.jna.platform.win32.Win32Exception;

/**
 * Native encrypter that uses Windows' native <a href="http://msdn.microsoft.com/en-us/library/ms995355.aspx">DPAPI</a> for encrypting and decrypting passwords.
 * <p>
 * 
 * Basically, DPAPI uses a hash of the currently logged in user's password (or other credential) to encrypt other passwords.
 * 
 * @author prunge
 */
@Component(role=NativeEncrypter.class, hint="windows")
public class WindowsDpApiEncrypter extends BinaryNativeEncrypter
{
	@Override
	public byte[] encryptBytes(byte[] binary)
	throws EncryptionException
	{
		if (binary == null)
			throw new NullPointerException("binary == null");
		
		try
		{
			byte[] encryptedBinary = Crypt32Util.cryptProtectData(binary);
			return(encryptedBinary);
		}
		catch (Win32Exception e)
		{
			throw new EncryptionException(e);
		}
	}

	@Override
	public byte[] decryptBytes(byte[] encryptedBinary)
	throws EncryptionException
	{
		if (encryptedBinary == null)
			throw new NullPointerException("encryptedBinary == null");
		
		try
		{
			byte[] binary = Crypt32Util.cryptUnprotectData(encryptedBinary);
			return(binary);
		}
		catch (Win32Exception e)
		{
			throw new EncryptionException(e);
		}
	}
	
	@Override
	public boolean isUsable()
	{
		return(Os.isFamily(Os.FAMILY_WINDOWS));
	}
	
	@Override
	public UsabilityDetail getUsabilityDetail()
	{
		if (!isUsable())
			return(new UsabilityDetail("not on Windows platform"));
		else
			return(null);
	}
}
