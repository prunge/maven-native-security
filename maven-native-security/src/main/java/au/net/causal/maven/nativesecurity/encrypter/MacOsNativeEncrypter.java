package au.net.causal.maven.nativesecurity.encrypter;

import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;
import org.codehaus.plexus.util.Os;
import org.sonatype.plexus.components.cipher.PlexusCipher;

import au.net.causal.maven.nativesecurity.encrypter.MacKeychain.KeychainNotAvailableException;

@Component(role=NativeEncrypter.class, hint="mac")
public class MacOsNativeEncrypter extends KeyBasedNativeEncrypter
{
	private final MacKeychain keychain;
	private final KeychainNotAvailableException notAvailableException;
	
	@Requirement
	private PlexusCipher cipher;
	
	public MacOsNativeEncrypter()
	{
		MacKeychain keychain;
		KeychainNotAvailableException notAvailableException;
		try
		{
			keychain = new MacKeychain("Maven Native Security master key");
			notAvailableException = null;
		}
		catch (KeychainNotAvailableException e)
		{
			notAvailableException = e;
			keychain = null;
		}
		
		this.keychain = keychain;
		this.notAvailableException = notAvailableException;
	}
	
	@Override
	public boolean isUsable()
	{
		return(getUsabilityDetail() == null);
	}

	@Override
	public UsabilityDetail getUsabilityDetail()
	{
		if (!Os.isFamily(Os.FAMILY_MAC))
			return(new UsabilityDetail("not on Mac-OS platform"));
		
		if (notAvailableException != null)
		{
			String summary;
			if (notAvailableException.getCause() instanceof UnsatisfiedLinkError)
				summary = "Gnome keyring library not found";
			else
				summary = notAvailableException.getMessage();
			
			return(new UsabilityDetail(summary, notAvailableException.toString(), notAvailableException));
		}
		else
			return(null);
	}

	@Override
	protected String readMasterKey() 
	throws EncryptionException
	{
		return(keychain.get(MASTER_KEY_NAME));
	}

	@Override
	protected void saveMasterKey(String masterKey) throws EncryptionException
	{
		keychain.put(MASTER_KEY_NAME, masterKey);
	}

	@Override
	protected PlexusCipher getCipher()
	{
		return(cipher);
	}

}
