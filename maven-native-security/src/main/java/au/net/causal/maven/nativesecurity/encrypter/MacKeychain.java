package au.net.causal.maven.nativesecurity.encrypter;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import au.net.causal.maven.nativesecurity.encrypter.MacNativeTypes.CFIndex;
import au.net.causal.maven.nativesecurity.encrypter.MacNativeTypes.CFRange;
import au.net.causal.maven.nativesecurity.encrypter.MacNativeTypes.CFStringRef;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

public class MacKeychain
{
	private static final String LIB_NAME = "Security";
	private static final String CF_LIB_NAME = "CoreFoundation";
	private static final List<String> ALT_LIB_NAMES = Collections.emptyList();
	private static final List<String> ALT_CF_LIB_NAMES = Collections.emptyList();
	
	private static final Charset UTF_8 = Charset.forName("UTF-8");
	
	private static final MacSecKeychainLibrary LIBRARY;
	private static final MacCoreFoundationLibrary CF_LIBRARY;
	private static final Throwable LIBRARY_INSTANTIATION_ERROR;
	
	private final String serviceName;
	
	static
	{
		MacSecKeychainLibrary library;
		MacCoreFoundationLibrary cfLibrary;
		Throwable libraryInstantiationError;
		try
		{
			library = loadLibrary(Collections.emptyMap());
			cfLibrary = loadCfLibrary(Collections.emptyMap());
			libraryInstantiationError = null;
		}
		catch (Throwable e)
		{
			library = null;
			cfLibrary = null;
			libraryInstantiationError = e;
		}
		
		LIBRARY = library;
		CF_LIBRARY = cfLibrary;
		LIBRARY_INSTANTIATION_ERROR = libraryInstantiationError;
	}
	
    private static MacSecKeychainLibrary loadLibrary(Map<?,?> options) 
    {
        try 
        {
            return (MacSecKeychainLibrary)Native.loadLibrary(LIB_NAME, MacSecKeychainLibrary.class, options);
        } 
        catch (UnsatisfiedLinkError e) 
        {
        	for (String altLibName : ALT_LIB_NAMES)
        	{
                if (new File(altLibName).isFile()) 
                    return (MacSecKeychainLibrary)Native.loadLibrary(altLibName, MacSecKeychainLibrary.class, options);
        	}
        	
        	//No alternatives found
        	throw e;
        }
    }
    
    private static MacCoreFoundationLibrary loadCfLibrary(Map<?,?> options) 
    {
        try 
        {
            return (MacCoreFoundationLibrary)Native.loadLibrary(CF_LIB_NAME, MacCoreFoundationLibrary.class, options);
        } 
        catch (UnsatisfiedLinkError e) 
        {
        	for (String altLibName : ALT_CF_LIB_NAMES)
        	{
                if (new File(altLibName).isFile()) 
                    return (MacCoreFoundationLibrary)Native.loadLibrary(altLibName, MacCoreFoundationLibrary.class, options);
        	}
        	
        	//No alternatives found
        	throw e;
        }
    }
    
    public MacKeychain()
    throws KeychainNotAvailableException
    {
    	this(MacKeychain.class.getCanonicalName());
    }
    
    public MacKeychain(String serviceName)
	throws KeychainNotAvailableException
	{
    	if (serviceName == null)
			throw new NullPointerException("serviceName == null");
    	
    	//Check if static initialization was successful
    	if (LIBRARY_INSTANTIATION_ERROR != null)
    		throw new KeychainNotAvailableException(LIBRARY_INSTANTIATION_ERROR);    	
    	
    	this.serviceName = serviceName;
	}
    
    public String get(String key)
    throws EncryptionException
    {
    	if (key == null)
			throw new NullPointerException("key == null");
    	
    	byte[] serviceNameBytes = serviceName.getBytes(UTF_8);
    	byte[] keyBytes = key.getBytes(UTF_8);
    	IntByReference passwordLengthRef = new IntByReference();
    	PointerByReference passwordRef = new PointerByReference();
    	int result = LIBRARY.SecKeychainFindGenericPassword(null, serviceNameBytes.length, serviceNameBytes, keyBytes.length, keyBytes, passwordLengthRef, passwordRef, null);
    	
    	checkResult(result, MacSecKeychainLibrary.errSecItemNotFound);
    	if (result == MacSecKeychainLibrary.errSecItemNotFound)
    		return(null);
    	if (passwordRef.getValue() == null)
    		return(null);
    	
    	try
    	{
    		int passwordLength = passwordLengthRef.getValue();
    		byte[] passwordBytes = passwordRef.getValue().getByteArray(0, passwordLength);
    		String password = new String(passwordBytes, UTF_8);
    		return(password);
    	}
    	finally
    	{
    		LIBRARY.SecKeychainItemFreeContent(null, passwordRef.getValue());    		
    	}
    }
    
    public void put(String key, String value)
    throws EncryptionException
    {
    	if (key == null)
			throw new NullPointerException("key == null");
    	if (value == null)
			throw new NullPointerException("value == null");
    	
    	PointerByReference itemRef = new PointerByReference();
    	byte[] serviceNameBytes = serviceName.getBytes(UTF_8);
    	byte[] keyBytes = key.getBytes(UTF_8);
    	byte[] passwordBytes = value.getBytes(UTF_8);
        int result = LIBRARY.SecKeychainFindGenericPassword(null, serviceNameBytes.length, serviceNameBytes,
        				keyBytes.length, keyBytes, null, null, itemRef);
        checkResult(result, MacSecKeychainLibrary.errSecItemNotFound);
    	if (result == MacSecKeychainLibrary.errSecItemNotFound)
    	{
    		result = LIBRARY.SecKeychainAddGenericPassword(null, serviceNameBytes.length, serviceNameBytes,
    							keyBytes.length, keyBytes, passwordBytes.length, passwordBytes, null);
    		checkResult(result);
    	}
    	else //it was found
    	{
    		//If it was found, replace 
    		Pointer item = itemRef.getValue();
    		if (item != null)
    		{
	            try 
	            {
	                result = LIBRARY.SecKeychainItemModifyAttributesAndData(item, null, passwordBytes.length, passwordBytes);
	                checkResult(result);
	            } 
	            finally
	            {
	            	//clean up itemRef
            		CF_LIBRARY.CFRelease(item);
	            }
    		}
    	}
    }
    
    private void checkResult(int result, int... allowedResults)
    throws EncryptionException
    {
    	if (result == 0)
    		return;
    	
    	for (int allowedResult : allowedResults)
    	{
    		if (result == allowedResult)
    			return;
    	}
    	
    	String errorMessage = errorMessage(result);
    	throw new EncryptionException("Error #" + result + ": " + errorMessage);
    }
    
    private String errorMessage(int result) 
    {
    	CFStringRef theString = LIBRARY.SecCopyErrorMessageString(result, null);
    	try 
    	{ 
    		return(decode(theString)); 
    	}
    	finally 
    	{ 
    		CF_LIBRARY.CFRelease(theString); 
    	}
    }
    
    private String decode(CFStringRef s) 
    {
    	int length = CF_LIBRARY.CFStringGetLength(s).intValue();
    	char[] buf = new char[length];
    	CF_LIBRARY.CFStringGetCharacters(s, new CFRange.ByValue(new CFIndex(0), new CFIndex(length)), buf);
    	return new String(buf);
    }
    
    public static class KeychainNotAvailableException extends Exception
    {
    	public KeychainNotAvailableException(String message)
    	{
    		super(message);
    	}
    	
    	public KeychainNotAvailableException(Throwable t)
    	{
    		super(t);
    	}
    }
}
