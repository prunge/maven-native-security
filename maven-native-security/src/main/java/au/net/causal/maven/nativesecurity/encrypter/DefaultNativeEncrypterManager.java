package au.net.causal.maven.nativesecurity.encrypter;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;

/**
 * Default implementation of the native encrypter manager that processes all registered <code>NativeEncrypter</code> components.
 * 
 * @author prunge
 */
@Component(role=NativeEncrypterManager.class, hint="default")
public class DefaultNativeEncrypterManager implements NativeEncrypterManager
{
	@Requirement
	private List<NativeEncrypter> encrypters;
	
	@Override
	public NativeEncrypter findUsableEncrypter()
	{
		for (NativeEncrypter encrypter : encrypters)
		{
			if (encrypter.isUsable())
				return(encrypter);
		}
		
		return(null);
	}
	
	@Override
	public List<NativeEncrypter> getEncrypters()
	{
		return(new ArrayList<NativeEncrypter>(encrypters));
	}
}
