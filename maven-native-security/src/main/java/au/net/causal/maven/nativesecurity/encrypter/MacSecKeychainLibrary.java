package au.net.causal.maven.nativesecurity.encrypter;

import au.net.causal.maven.nativesecurity.encrypter.MacNativeTypes.CFStringRef;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

/**
 * @see <a href="https://developer.apple.com/library/mac/documentation/security/Reference/keychainservices/Reference/reference.html">SecKeychain library reference</a>
 */
public interface MacSecKeychainLibrary extends Library
{	
	int errSecItemNotFound = -25300;
	
	int SecKeychainFindGenericPassword(Pointer /*CFTypeRef*/ keychainOrArray, int serviceNameLength, byte[] serviceName,
            int accountNameLength, byte[] accountName, IntByReference passwordLength,
            PointerByReference passwordData, PointerByReference itemRef);
	
	int SecKeychainItemFreeContent(Pointer attrList, Pointer data);
	
	int SecKeychainAddGenericPassword(Pointer keychain, int serviceNameLength, byte[] serviceName, int accountNameLength, byte[] accountName, 
			int passwordLength, byte[] passwordData, PointerByReference itemRef);
	
	int SecKeychainItemModifyAttributesAndData(Pointer itemRef, PointerByReference attrList, int length, byte[] data);
	
	CFStringRef SecCopyErrorMessageString(int status, Pointer reserved);
}
