package au.net.causal.maven.nativesecurity.encrypter;

import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;
import org.sonatype.plexus.components.cipher.PlexusCipher;

import au.net.causal.maven.nativesecurity.encrypter.GnomeKeyring.KeyringNotAvailableException;

@Component(role=NativeEncrypter.class, hint="gnome")
public class GnomeKeyringNativeEncrypter extends KeyBasedNativeEncrypter
{
	private static final String APP_NAME = "Maven Native Security";
	
	private final GnomeKeyring keyring;
	private final KeyringNotAvailableException notAvailableException;
	
	@Requirement
	private PlexusCipher cipher;
	
	public GnomeKeyringNativeEncrypter()
	{
		GnomeKeyring keyring;
		KeyringNotAvailableException notAvailableException;
		try
		{
			keyring = new GnomeKeyring(APP_NAME);
			notAvailableException = null;
		}
		catch (KeyringNotAvailableException e)
		{
			notAvailableException = e;
			keyring = null;
		}
		
		this.keyring = keyring;
		this.notAvailableException = notAvailableException;
	}
	
	@Override
	public boolean isUsable()
	{
		if (notAvailableException != null)
			getLogger().debug("Gnome keyring not available: " + notAvailableException, notAvailableException);
		return(keyring != null);
	}
	
	@Override
	public UsabilityDetail getUsabilityDetail()
	{
		if (isUsable())
			return(null);
		
		String summary;
		if (notAvailableException.getCause() instanceof UnsatisfiedLinkError)
			summary = "Gnome keyring library not found";
		else
			summary = notAvailableException.getMessage();
		
		return(new UsabilityDetail(summary, notAvailableException.toString(), notAvailableException));
	}

	@Override
	protected String readMasterKey() 
	throws EncryptionException
	{
		return(keyring.get(MASTER_KEY_NAME));
	}

	@Override
	protected void saveMasterKey(String masterKey) throws EncryptionException
	{
		keyring.put(MASTER_KEY_NAME, masterKey, "Master key for Maven Native Security");
	}

	@Override
	protected PlexusCipher getCipher()
	{
		return(cipher);
	}
}
