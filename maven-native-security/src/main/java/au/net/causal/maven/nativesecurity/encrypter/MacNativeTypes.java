package au.net.causal.maven.nativesecurity.encrypter;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.PointerType;
import com.sun.jna.Structure;

public class MacNativeTypes
{
	public static class CFStringRef extends PointerType 
	{
		public CFStringRef() 
		{
		}
		
		public CFStringRef(Pointer p)
		{ 
			super(p); 
		}
	}
	
	public static class CFIndex extends NativeLong 
	{
		public CFIndex() 
		{
		}
		
		public CFIndex(long value) 
		{
			super(value); 
		}
	}
	
	public static class CFRange extends Structure 
	{
		public CFIndex location;
		public CFIndex length;
		
		public CFRange() 
		{
		}

		public CFRange(CFIndex location, CFIndex length) 
		{
			this.location = location;
			this.length = length;
		}
		
		@Override
		protected List<String> getFieldOrder() 
		{
			return Arrays.asList("location", "length");
		}

		public static class ByValue extends CFRange implements Structure.ByValue 
		{
			public ByValue() 
			{
			}

			public ByValue(CFIndex location, CFIndex length) 
			{
				super(location, length);
			}
		}
    }
}
