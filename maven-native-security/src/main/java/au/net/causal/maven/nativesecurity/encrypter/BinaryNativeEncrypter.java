package au.net.causal.maven.nativesecurity.encrypter;

import java.nio.charset.Charset;

import org.codehaus.plexus.util.Base64;

/**
 * Native encrypters that work with raw bytes instead of base64-encoded strings extend this class.
 * UTF-8 is used to convert between characters and bytes.
 * 
 * @author prunge
 */
public abstract class BinaryNativeEncrypter implements NativeEncrypter
{
	private static final Charset UTF_8 = Charset.forName("UTF-8");
	
	@Override
	public String encrypt(String password) 
	throws EncryptionException
	{
		if (password == null)
			throw new NullPointerException("password == null");
		
		byte[] passwordBytes = password.getBytes(UTF_8);
		byte[] encryptedBytes = encryptBytes(passwordBytes);
		byte[] base64EncryptedBytes = Base64.encodeBase64(encryptedBytes);
		
		return(new String(base64EncryptedBytes, UTF_8));
	}
	
	@Override
	public String decrypt(String base64EncodedPassword) 
	throws EncryptionException
	{
		if (base64EncodedPassword == null)
			throw new NullPointerException("base64EncodedPassword == null");
		
		byte[] base64EncryptedBytes = base64EncodedPassword.getBytes(UTF_8);
		byte[] encryptedPasswordBytes = Base64.decodeBase64(base64EncryptedBytes);
		byte[] passwordBytes = decryptBytes(encryptedPasswordBytes);
		
		return(new String(passwordBytes, UTF_8));
	}
	
	/**
	 * Encrypts a password.
	 * 
	 * @param passwordBytes the unencrypted bytes of the password to encrypt.
	 * 
	 * @return the encrypted form of the password.
	 * 
	 * @throws NullPointerException if <code>passwordBytes</code> is null.
	 * @throws EncryptionException if an error occurs performing encryption.
	 */
	protected abstract byte[] encryptBytes(byte[] passwordBytes)
	throws EncryptionException;
	
	/**
	 * Decrypts a password.
	 * 
	 * @param encryptedPassword the encrypted bytes of the password to decrypt.
	 * 
	 * @return the unencrypted bytes of the password.
	 * 
	 * @throws NullPointerException if <code>encryptedPassword</code> is null.
	 * @throws EncryptionException if an error occurs performing decryption.
	 */
	protected abstract byte[] decryptBytes(byte[] encryptedPassword)
	throws EncryptionException;
}
