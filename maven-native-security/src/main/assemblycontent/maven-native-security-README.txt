${project.name}
-----------------------------

To install this extension, copy all files from the install ZIP into [maven installation directory]/lib/ext.  This includes:

the main extension file ${project.build.finalName}, as well as the dependencies:
${readme.classpath}
