package au.net.causal.maven.nativesecurity.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.codehaus.plexus.components.interactivity.Prompter;
import org.codehaus.plexus.components.interactivity.PrompterException;

import au.net.causal.maven.nativesecurity.encrypter.EncryptionException;
import au.net.causal.maven.nativesecurity.encrypter.NativeEncrypter;
import au.net.causal.maven.nativesecurity.encrypter.NativeEncrypterManager;

/**
 * Encrypts passwords using Maven native security using the native system's security facilities and prints the encrypted password to the console so that the
 * user can copy this into their <code>settings.xml</code>.
 * 
 * @author prunge
 */
@Mojo(name="encrypt-password", requiresDirectInvocation=true, requiresProject=false)
public class EncryptPasswordMojo extends AbstractNativeSecurityMojo
{
	@Component
	private Prompter fallbackPrompter;
	
	@Component(hint="secure")
	private Prompter securePrompter;
	
	@Component
	private NativeEncrypterManager encrypterManager;
	
	@Override
	public void executeImpl() 
	throws MojoExecutionException, MojoFailureException
	{
		NativeEncrypter encrypter = encrypterManager.findUsableEncrypter();
		if (encrypter == null)
			throw new MojoExecutionException("No native encrypters available for current platform.");
		
		try
		{
			String password;
			try
			{
				//For the secure non-echoing prompter, prompt twice and verify
				password = securePrompter.promptForPassword("Password");
				String passwordVerification = securePrompter.promptForPassword("Re-enter password");
				
				if (password != null && !password.equals(passwordVerification))
					throw new MojoFailureException("Entered passwords do not match.");
			}
			catch (PrompterException e)
			{
				//Don't need to prompt twice when password is echoed
				getLog().warn("Secure password prompter not available, entered password will be echoed to console!");
				password = fallbackPrompter.promptForPassword("Password");
			}
			
			//Prompt for password
			if (password == null || password.isEmpty())
				throw new MojoFailureException("No password entered");
			
			String encryptedBase64String = encrypter.encrypt(password);
			
			//Add our magic around the raw data
			String fullString = "{[type=native]" + encryptedBase64String + "}";
			
			//And display the result to the user
			getLog().info("Use the following string in your settings:");
			getLog().info(fullString);
			
		}
		catch (PrompterException e)
		{
			throw new MojoExecutionException("Error prompting for password: " + e, e);
		}
		catch (EncryptionException e)
		{
			throw new MojoExecutionException("Failed to encrypt password: " + e.getMessage(), e);
		}
	}
}
