package au.net.causal.maven.nativesecurity.plugin;

import java.io.Console;
import java.util.List;

import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;
import org.codehaus.plexus.components.interactivity.Prompter;
import org.codehaus.plexus.components.interactivity.PrompterException;

/**
 * A prompter that reads passwords without echoing the password back to the user.  The standard prompter implementation seems to echo passwords all the time.
 * All other methods delegate to the default prompter implementation.
 * <p>
 * 
 * This prompter may not always be usable - it relies on {@link Console#readPassword()} which is not available in all environments.  When a console is not
 * available, a {@link PrompterException} is thrown from {@link #promptForPassword(String)}.
 * 
 * @author prunge
 */
@Component(role=Prompter.class, hint="secure")
public class SecurePrompter implements Prompter
{
	@Requirement
	private Prompter delegate;
	
	@Override
	public String promptForPassword(String message) 
	throws PrompterException
	{
		Console console = System.console();
		if (console == null)
			throw new PrompterException("Console not available.");
		
		showMessage(message);
		
		char[] pass = console.readPassword();
		if (pass == null)
			return(null);
		else
			return(new String(pass));
	}

	//If Plexus annotation metadata generator supported inheritence the delegate would not be necessary
	
	@Override
	public String prompt(String message) throws PrompterException
	{
		return delegate.prompt(message);
	}

	@Override
	public String prompt(String message, String defaultReply) throws PrompterException
	{
		return delegate.prompt(message, defaultReply);
	}

	@Override
	public String prompt(String message, List possibleValues, String defaultReply) throws PrompterException
	{
		return delegate.prompt(message, possibleValues, defaultReply);
	}

	@Override
	public String prompt(String message, List possibleValues) throws PrompterException
	{
		return delegate.prompt(message, possibleValues);
	}

	@Override
	public void showMessage(String message) throws PrompterException
	{
		delegate.showMessage(message);
	}
}
