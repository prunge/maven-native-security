package au.net.causal.maven.nativesecurity.plugin;

import java.util.List;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import au.net.causal.maven.nativesecurity.encrypter.NativeEncrypter;
import au.net.causal.maven.nativesecurity.encrypter.NativeEncrypter.UsabilityDetail;
import au.net.causal.maven.nativesecurity.encrypter.NativeEncrypterManager;

/**
 * Displays all installed providers of native security along with their usability on the current platform.
 * 
 * @author prunge
 */
@Mojo(name="providers", requiresDirectInvocation=true, requiresProject=false)
public class ProviderDetailMojo extends AbstractNativeSecurityMojo
{
	@Component
	private NativeEncrypterManager encrypterManager;
	
	/**
	 * If true, additional details for encrypter unavailablility is displayed. 
	 */
	@Parameter(property="natsec.verboseDetail")
	private boolean verboseEncrypterDetail;
	
	@Override
	public void executeImpl() 
	throws MojoExecutionException, MojoFailureException
	{
		List<NativeEncrypter> encrypters = encrypterManager.getEncrypters();
		
		getLog().info("Encrypters:");
		for (NativeEncrypter encrypter : encrypters)
		{
			boolean usable = encrypter.isUsable();
			UsabilityDetail usabilityDetail = encrypter.getUsabilityDetail();
			String reason;
			if (usabilityDetail == null)
				reason = null;
			else
			{
				if (verboseEncrypterDetail && usabilityDetail.getDetailMessage() != null)
					reason = usabilityDetail.getDetailMessage();
				else
					reason = usabilityDetail.getSummary();
			}
			
			if (reason == null)
				reason = "(no reason given)";
			
			String detail = (usable ? "usable" : "not usable - " + reason);
			getLog().info(encrypter.getClass().getCanonicalName() + " - " + detail);
			
			if (usabilityDetail != null && usabilityDetail.getError() != null)
				getLog().debug(usabilityDetail.getError());
		}
	}
}
