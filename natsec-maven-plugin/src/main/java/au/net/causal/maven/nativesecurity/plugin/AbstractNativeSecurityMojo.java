package au.net.causal.maven.nativesecurity.plugin;

import java.util.regex.Pattern;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;

public abstract class AbstractNativeSecurityMojo extends AbstractMojo
{
	private static final int MIN_MAVEN_MAJOR_VERSION = 3;
	private static final int MIN_MAVEN_MINOR_VERSION = 1;
	
	@Parameter(defaultValue="${maven.version}", readonly=true)
	private String mavenVersion;
	
	/**
	 * If true, fails execution of the plugin if the Maven version is too old.  If set to false, only a warning is displayed in this case.
	 */
	@Parameter(defaultValue="true", property="natsec.enforceMavenVersionCheck")
	private boolean enforceMavenVersionCheck;
	
	/**
	 * Checks the Maven version being run against and warns or fails if it is too early.
	 * 
	 * @throws MojoExecutionException if version enforcement is enabled and the check fails.
	 */
	protected void checkMavenVersion()
	throws MojoExecutionException
	{
		//Parse into major.minor[.anything]
		String[] versionComponents = mavenVersion.split(Pattern.quote("."));
		if (versionComponents.length < 2)
		{
			getLog().warn("Failed to parse Maven version '" + mavenVersion + "'.");
			return;
		}
		
		try
		{
			int major = Integer.parseInt(versionComponents[0]); 
			int minor = Integer.parseInt(versionComponents[1]);
			
			boolean tooOld;
			if (major < MIN_MAVEN_MAJOR_VERSION)
				tooOld = true;
			else if (major == MIN_MAVEN_MAJOR_VERSION && minor < MIN_MAVEN_MINOR_VERSION)
				tooOld = true;
			else
				tooOld = false;
			
			if (tooOld)
			{
				String message ="Maven version '" + mavenVersion + "' is too old.  This plugin should be run with version " + MIN_MAVEN_MAJOR_VERSION + "." + 
									MIN_MAVEN_MINOR_VERSION + " or later.";
				
				if (enforceMavenVersionCheck)
					throw new MojoExecutionException(message);
				else
					getLog().warn(message);
			}
				
		}
		catch (NumberFormatException e)
		{
			getLog().warn("Failed to parse Maven version '" + mavenVersion + "'.", e);
		}
	}
	
	@Override
	public final void execute() throws MojoExecutionException, MojoFailureException
	{
		checkMavenVersion();
		executeImpl();
	}
	
	protected abstract void executeImpl()
	throws MojoExecutionException, MojoFailureException;
}
